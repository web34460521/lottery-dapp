import { createContext, useContext, useEffect, useMemo, useState } from "react";
import { BN } from "@project-serum/anchor";
import { SystemProgram, LAMPORTS_PER_SOL, Connection } from "@solana/web3.js";
import { useAnchorWallet, useConnection } from "@solana/wallet-adapter-react";
import bs58 from "bs58";

import {
  getLotteryAddress,
  getMasterAddress,
  getProgram,
  getTicketAddress,
  getTotalPrize,
} from "../utils/program";
import { mockWallet, confirmTx } from "../utils/helper";
import toast from "react-hot-toast";

export const AppContext = createContext();

export const AppProvider = ({ children }) => {
  const [masterAddress, setMasterAddress] = useState();
  const [initialized, setInitialized] = useState(false);
  const [lotteryId, setLotteryId] = useState();
  const [lotteryPot, setLotteryPot] = useState(0);
  const [lottery, setLottery] = useState();
  const [lotteryAddress, setLotteryAddress] = useState();
  const [userWinningId, setUserWinningId] = useState();
  const [lotteryHistory, setLotteryHistory] = useState([]);

  const { connection } = useConnection();
  // (async () => {
  //   let log = await connection.getLatestBlockhash();
  //   console.log(log);
  // })();
  const wallet = useAnchorWallet();
  const program = useMemo(
    () => connection && getProgram(connection, wallet ?? mockWallet()),
    [connection, wallet],
  );
  // console.log(program);

  const updateState = async () => {
    if (!program) return;

    try {
      if (!masterAddress) {
        const masterAddress = await getMasterAddress();
        setMasterAddress(masterAddress);
      }
      const master = await program.account.master.fetch(
        masterAddress ?? (await getMasterAddress()),
      );

      setLotteryId(master.lastId);
      setInitialized(true);
      const lotteryAddress = await getLotteryAddress(master.lastId);
      setLotteryAddress(lotteryAddress);
      const lottery = await program.account.lottery.fetch(lotteryAddress);
      setLottery(lottery);

      // Get user's ticket's for the current lottery
      if (!wallet?.publicKey) return;
      // const userTickets = await program.account.ticket.all([
      //   {
      //     memcmp: {
      //       bytes: bs58.encode(new BN(lotteryId).toArrayLike(Buffer, "le", 4)),
      //       offset: 12,
      //     },
      //   },
      //   // { memcmp: { bytes: wallet.publicKey.toBase58(), offset: 16 } },
      // ]);
      // // console.log(userTickets);

      // // Check wheather any of the user tickets win
      // const userWin = userTickets.some(
      //   (t) => t.account.id === lottery.winnerId,
      // );

      // if (userWin) {
      //   // console.log(lottery.winnerId);
      //   setUserWinningId(lottery.winnerId);
      // } else {
      //   setUserWinningId(null);
      // }
      setUserWinningId(lottery.winnerId);
    } catch (err) {
      console.log(err.message);
    }
  };

  const getPot = async () => {
    const pot = getTotalPrize(lottery);
    setLotteryPot(pot);
  };

  const getLotteryHistory = async () => {
    const history = Promise.all(
      new Array(lotteryId).fill(null).map(async (_, i) => {
        const id = lotteryId - i;
        const lotteryAddress = await getLotteryAddress(id);
        const lottery = await program.account.lottery.fetch(lotteryAddress);
        const winnerId = lottery.winnerId;

        if (!winnerId) return null;

        const ticketAddress = await getTicketAddress(lotteryAddress, winnerId);

        return {
          lotteryId: id,
          winnerId: winnerId,
          winnerAddress: (await program.account.ticket.fetch(ticketAddress))
            .authority,
          prize: getTotalPrize(lottery),
        };
      }),
    ).then((d) => d.filter((d) => d != null));
    setLotteryHistory(await history);
  };

  useEffect(() => {
    updateState();
  }, [program]);

  useEffect(() => {
    if (!lottery) return;
    getPot();
    getLotteryHistory();
    //console.log(lotteryHistory);
  }, [lottery]);

  const initMaster = async () => {
    try {
      const tx = await program.methods
        .initMaster()
        .accounts({
          master: masterAddress ?? (await getMasterAddress()),
          payer: wallet.publicKey,
          systemProgram: SystemProgram.programId,
        })
        .rpc();
      const txHash = tx.toString();
      await confirmTx(txHash, connection);
      await updateState();
      toast.success("Initialized Master!");
    } catch (err) {
      console.log(err.message);
      toast.error(err.message);
    }
  };

  const createLottery = async () => {
    try {
      const lotteryAddress = await getLotteryAddress(lotteryId + 1); //PublicKey
      console.log(lotteryId);
      console.log(lotteryAddress.toString());
      console.log(masterAddress.toString());
      const tx = await program.methods
        .createLottery(new BN(1).mul(new BN(LAMPORTS_PER_SOL)))
        .accounts({
          lottery: lotteryAddress,
          master: masterAddress,
          authority: wallet.publicKey,
          systemProgram: SystemProgram.programId,
        })
        .rpc();
      await confirmTx(tx.toString(), connection);
      toast.success("Lottery is created!");

      const lottery = await program.account.lottery.fetch(lotteryAddress);
      setLottery(lottery);
    } catch (err) {
      console.log(err.message);
      toast.error(err.message);
    }
  };

  const buyTicket = async () => {
    try {
      console.log(lotteryAddress);
      const tx = await program.methods
        .buyTicket(lotteryId)
        .accounts({
          lottery: lotteryAddress,
          ticket: await getTicketAddress(
            lotteryAddress,
            lottery.lastTicketId + 1,
          ),
          buyer: wallet.publicKey,
          systemProgram: SystemProgram.programId,
        })
        .rpc();
      await confirmTx(tx.toString(), connection);
      toast.success("Ticket is bought!");
      let updatedLottery = await program.account.lottery.fetch(lotteryAddress);
      setLottery(updatedLottery);
      //getPot();
    } catch (err) {
      console.log(err.message);
      toast.error(err.message);
    }
  };

  const pickWinner = async () => {
    try {
      const tx = await program.methods
        .pickWinner(lotteryId)
        .accounts({
          lottery: lotteryAddress,
          authority: wallet.publicKey,
        })
        .rpc();
      await confirmTx(tx.toString(), connection);
      updateState();
      toast.success("Winner is picked!");
    } catch (err) {
      console.log(err.message);
      toast.error(err.message);
    }
  };

  const claimPrize = async () => {
    try {
      const txHash = await program.methods
        .claimPrize(lotteryId, userWinningId)
        .accounts({
          lottery: lotteryAddress,
          ticket: await getTicketAddress(lotteryAddress, userWinningId),
          authority: wallet.publicKey,
          systemProgram: SystemProgram.programId,
        })
        .rpc();
      await confirmTx(txHash, connection);
      updateState();
      toast.success("Claimed the prize!!");
    } catch (err) {
      toast.error(err.message);
    }
  };

  return (
    <AppContext.Provider
      value={{
        // Put functions/variables you want to bring out of context to App in here
        connected: wallet?.publicKey ? true : false,
        isMasterInitialized: initialized,
        initMaster,
        createLottery,
        lotteryId,
        lotteryPot,
        isLotteryAuthority:
          !!wallet && !!lottery && wallet.publicKey.equals(lottery.authority),
        isFinished: !!lottery && !!userWinningId,
        canClaim: !!lottery && !lottery.claimed && !!userWinningId,
        buyTicket,
        pickWinner,
        lotteryHistory,
        claimPrize,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => {
  return useContext(AppContext);
};
