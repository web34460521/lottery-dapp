/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    RPC_NODE: "https://solana-devnet.g.alchemy.com/v2/" + process.env.API_KEY,
    //"https://rpc.ankr.com/solana_devnet",
    PROGRAM_ID: process.env.PROGRAM_ID,
  },
};

module.exports = nextConfig;
